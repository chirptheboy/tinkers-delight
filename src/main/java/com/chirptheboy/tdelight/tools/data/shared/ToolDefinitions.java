package com.chirptheboy.tdelight.tools.data.shared;

import slimeknights.tconstruct.library.tools.ToolDefinition;

public class ToolDefinitions {

    public static final ToolDefinition MACE = ToolDefinition.builder(DelightTools.mace).meleeHarvest().build();
    public static final ToolDefinition NAGINATA = ToolDefinition.builder(DelightTools.naginata).meleeHarvest().build();
    public static final ToolDefinition WAR_HAMMER = ToolDefinition.builder(DelightTools.warHammer).meleeHarvest().build();

}
